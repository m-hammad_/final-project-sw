Feature: display list of services that could be done by a manager account

  As a company manager
  So that I can browse my services
  I want to see the services that my account can work with

  Background: User has been logged in
    And  I am on the AccPro home page

  Scenario: Dashboard content
    When I am on the AccPro home page
    And I fill in "username_or_email" with "hammad77@gmail.com"
    And I fill in "login_password" with "123456"
    And I press "Log In"
    Then I should see "View Transactions History"
    And I should see "Generate Balance Sheet"
    And I should see "Create/Edit/Delete A User"
    And I should see "Create/Edit/Delete An Account"
