Feature: display list of users

  As a company manager
  So that I can browse the users' accounts
  I want to see a list of the users I have in my database

  Background: User has been logged in
    And  I am on the users page

  Scenario: Accounts list content
    When I am on the users page
    And I fill in "username_or_email" with "hammad77@gmail.com"
    And I fill in "login_password" with "123456"
    And I press "Log In"
    And I am on the users page
    Then I should see "Listing users"
    And I should see "Logout"
    And I should see "Profile"
