Feature: display list of TXNs

  As a company manager
  So that I can browse the TXNs
  I want to see a list of TXNs that have been recorded by my accountants

  Background: User has been logged in
    And  I am on the transactions page

  Scenario: Transaction list content
    When I am on the transactions page
    And I fill in "username_or_email" with "hammad77@gmail.com"
    And I fill in "login_password" with "123456"
    And I press "Log In"
    And I am on the transactions page
    Then I should see "Listing acc_transactions"
    And I should see "Logout"
    And I should see "Profile"
