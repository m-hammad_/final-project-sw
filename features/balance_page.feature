Feature: display balance sheet

  As a company manager
  So that I can generate a balance sheet
  I want to see a list of the accounts, each with its balance

  Background: User has been logged in
    And  I am on the balance sheet page

  Scenario: Balance sheet content
    When I am on the balance sheet page
    And I fill in "username_or_email" with "hammad77@gmail.com"
    And I fill in "login_password" with "123456"
    And I press "Log In"
    And I am on the balance sheet page
    Then I should see "The current balances sheet"
    And I should see "Home Page"
    And I should see "Logout"
    And I should see "Profile"
