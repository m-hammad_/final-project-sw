Feature: Login to the system

  As a user
  So that I see my services
  I want to login to the system

  Background: User has an email and password
    And  I am on the login page

  Scenario: login page fields
    When I am on the login page
    Then I should see "Email"
    And I should see "Password"

  Scenario: login page action
    When I am on the login page
    And I fill in "username_or_email" with "hammad77@gmail.com"
    And I fill in "login_password" with "123456"
    And I press "Log In"
    Then I should see "Welcome"
    And I should see "Logout"
    And I should see "Profile"
