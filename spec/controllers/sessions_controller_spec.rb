require 'rails_helper'

RSpec.describe SessionsController, :type => :controller do

  let(:valid_session) { {} }

  describe "GET login" do
    it "returns http success" do
      get :login, {}, valid_session
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET logout" do
    it "returns http success" do
      # user = User.authenticate('hammad77@gmail.com', '123456')
      # session[:user_id] = user.id
      get :logout, {}, valid_session
      expect(response).to have_http_status(302)
    end
  end

  # describe "GET home" do
  #   it "returns http success" do
  #     get :home, {}, valid_session
  #     expect(response).to have_http_status(:success)
  #   end
  # end
  #
  # describe "GET profile" do
  #   it "returns http success" do
  #     get :profile, {}, valid_session
  #     expect(response).to have_http_status(:success)
  #   end
  # end
  #
  # describe "GET setting" do
  #   it "returns http success" do
  #     get :setting, {}, valid_session
  #     expect(response).to have_http_status(:success)
  #   end
  # end

end
