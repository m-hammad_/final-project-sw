require 'rails_helper'

RSpec.describe UserGroup, :type => :model do
  let(:user_group) { FactoryGirl.create(:user_group) }

  it { should respond_to(:name) }
  it { should respond_to(:desc) }

end
