require 'rails_helper'

RSpec.describe Account, :type => :model do

  let(:account) { FactoryGirl.create(:account) }

  it { should respond_to(:name) }
  it { should respond_to(:account_type_id) }
  it { should respond_to(:balance) }

  it { should be_valid }

  describe "Seeding the DB" do
    it "should return 4 for number of Account records" do
      expect(Account.count).to eq(4)
    end

    it "should have the elements of the first Account correct" do
      e1= Account.first
      expect(e1.name).to eq('Cash')
      expect(e1.account_type_id).to eq(1)
      expect(e1.balance).to eq(0)
    end
  end


  describe "Updating accounts balances" do
    it "should update the balance of the accounts that have been sent to update_balances method" do
      Account.update_balances('to', 4, 'to', 2, 100)
      acc_a = Account.find(4)
      acc_b = Account.find(2)
      expect(acc_a.balance).to eq(300)
      expect(acc_b.balance).to eq(300)
    end
  end

end
