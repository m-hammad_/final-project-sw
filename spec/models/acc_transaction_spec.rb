require 'rails_helper'

RSpec.describe AccTransaction, :type => :model do

  let(:acc_transaction) { FactoryGirl.create(:acc_transaction) }

  it { should respond_to(:type_a) }
  it { should respond_to(:type_a_acc) }
  it { should respond_to(:type_b) }
  it { should respond_to(:type_b_acc) }
  it { should respond_to(:amount) }
  it { should respond_to(:date) }
  it { should respond_to(:desc) }

  it { should be_valid }

  describe "Seeding the DB" do
    it "should return 2 for number of TXN records" do
      expect(AccTransaction.count).to eq(2)
    end

    it "should have the elements of the first TXN correct" do
      e1= AccTransaction.first
      expect(e1.type_a_acc).to eq(4)
      expect(e1.type_b_acc).to eq(2)
      expect(e1.amount).to eq(100)
    end
  end

  # describe "Validates TXN" do
  #   it "should not save the TXN if not valid" do
  #     succ = AccTransaction.create(type_a: 'to', type_a_acc: 4, type_b: 'to', type_b_acc: 2, amount: 111, date: '2015-01-15', desc: 'Cash withdrawal')
  #     expect(succ).to be_false
  #   end
  # end

end
