require 'rails_helper'

RSpec.describe User, :type => :model do
  let(:user) { FactoryGirl.create(:user) }

  it { should respond_to(:name) }
  it { should respond_to(:email) }
  it { should respond_to(:group_id) }
  it { should respond_to(:encrypted_password) }
  it { should respond_to(:salt) }

  # it { should be_valid }

  describe "Seeding the DB" do
    it "should return 2 for number of User records" do
      expect(User.count).to eq(2)
    end

    it "should have the elements of the first User correct" do
      e1= User.first
      expect(e1.name).to eq('Hammad')
      expect(e1.email).to eq('hammad77@gmail.com')
      expect(e1.password).to eq(nil)
    end
  end
end
