require 'rails_helper'

RSpec.describe AccountType, :type => :model do
  let(:account_type) { FactoryGirl.create(:account_type) }

  it { should respond_to(:name) }
  it { should respond_to(:lhs_attr) }
  it { should respond_to(:rhs_attr) }
  it { should respond_to(:plus_attr) }
  it { should respond_to(:minus_attr) }

end
