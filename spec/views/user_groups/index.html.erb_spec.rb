require 'rails_helper'

RSpec.describe "user_groups/index", :type => :view do
  before(:each) do
    assign(:user_groups, [
      UserGroup.create!(
        :name => "Name",
        :desc => "Desc"
      ),
      UserGroup.create!(
        :name => "Name",
        :desc => "Desc"
      )
    ])
  end

  it "renders a list of user_groups" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Desc".to_s, :count => 2
  end
end
