require 'rails_helper'

RSpec.describe "acc_transactions/show", :type => :view do
  before(:each) do
    @acc_transaction = assign(:acc_transaction, AccTransaction.create!(
      :type_a => "Type A",
      :type_a_acc => 1,
      :type_b => "Type B",
      :type_b_acc => 2,
      :amount => 1.5,
      :desc => "Desc"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Type A/)
    expect(rendered).to match(/1/)
    expect(rendered).to match(/Type B/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/1.5/)
    expect(rendered).to match(/Desc/)
  end
end
