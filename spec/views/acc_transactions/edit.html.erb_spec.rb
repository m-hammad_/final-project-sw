require 'rails_helper'

RSpec.describe "acc_transactions/edit", :type => :view do
  before(:each) do
    @acc_transaction = assign(:acc_transaction, AccTransaction.create!(
      :type_a => "MyString",
      :type_a_acc => 1,
      :type_b => "MyString",
      :type_b_acc => 1,
      :amount => 1.5,
      :desc => "MyString"
    ))
  end

  it "renders the edit acc_transaction form" do
    render

    assert_select "form[action=?][method=?]", acc_transaction_path(@acc_transaction), "post" do

      assert_select "input#acc_transaction_type_a[name=?]", "acc_transaction[type_a]"

      assert_select "input#acc_transaction_type_a_acc[name=?]", "acc_transaction[type_a_acc]"

      assert_select "input#acc_transaction_type_b[name=?]", "acc_transaction[type_b]"

      assert_select "input#acc_transaction_type_b_acc[name=?]", "acc_transaction[type_b_acc]"

      assert_select "input#acc_transaction_amount[name=?]", "acc_transaction[amount]"

      assert_select "input#acc_transaction_desc[name=?]", "acc_transaction[desc]"
    end
  end
end
