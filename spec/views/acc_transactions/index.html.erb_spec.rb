require 'rails_helper'

RSpec.describe "acc_transactions/index", :type => :view do
  before(:each) do
    assign(:acc_transactions, [
      AccTransaction.create!(
        :type_a => "Type A",
        :type_a_acc => 1,
        :type_b => "Type B",
        :type_b_acc => 2,
        :amount => 1.5,
        :desc => "Desc"
      ),
      AccTransaction.create!(
        :type_a => "Type A",
        :type_a_acc => 1,
        :type_b => "Type B",
        :type_b_acc => 2,
        :amount => 1.5,
        :desc => "Desc"
      )
    ])
  end

  it "renders a list of acc_transactions" do
    render
    assert_select "tr>td", :text => "Type A".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Type B".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => "Desc".to_s, :count => 2
  end
end
