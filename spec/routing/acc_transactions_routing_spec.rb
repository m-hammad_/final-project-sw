require "rails_helper"

RSpec.describe AccTransactionsController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/acc_transactions").to route_to("acc_transactions#index")
    end

    it "routes to #new" do
      expect(:get => "/acc_transactions/new").to route_to("acc_transactions#new")
    end

    it "routes to #show" do
      expect(:get => "/acc_transactions/1").to route_to("acc_transactions#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/acc_transactions/1/edit").to route_to("acc_transactions#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/acc_transactions").to route_to("acc_transactions#create")
    end

    it "routes to #update" do
      expect(:put => "/acc_transactions/1").to route_to("acc_transactions#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/acc_transactions/1").to route_to("acc_transactions#destroy", :id => "1")
    end

  end
end
