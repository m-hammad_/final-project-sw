class CreateAccTransactions < ActiveRecord::Migration
  def change
    create_table :acc_transactions do |t|
      t.string :type_a
      t.integer :type_a_acc
      t.string :type_b
      t.integer :type_b_acc
      t.float :amount
      t.date :date
      t.string :desc

      t.timestamps
    end
  end
end
