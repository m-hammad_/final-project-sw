# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# Initial Data
# Account Types
AccountType.create(name: 'Asset', lhs_attr: true, rhs_attr: false, plus_attr: true, minus_attr:false)
AccountType.create(name: 'Liability', lhs_attr: true, rhs_attr: false, plus_attr: false, minus_attr:true)
AccountType.create(name: 'Expense', lhs_attr: false, rhs_attr: true, plus_attr: false, minus_attr:true)
AccountType.create(name: 'Equity', lhs_attr: false, rhs_attr: true, plus_attr: true, minus_attr:false)
# Accounts
Account.create(name: 'Cash', account_type_id: 1, balance: 0)
Account.create(name: 'Bank', account_type_id: 1, balance: 200)
Account.create(name: 'Office Expenses', account_type_id: 3, balance: 0)
Account.create(name: 'Capital', account_type_id: 4, balance: 200)
# TXNs
AccTransaction.create(type_a: 'to', type_a_acc: 4, type_b: 'to', type_b_acc: 2, amount: 100, date: '2015-01-15', desc: 'Cash withdrawal')
AccTransaction.create(type_a: 'to', type_a_acc: 4, type_b: 'to', type_b_acc: 2, amount: 100, date: '2015-01-15', desc: 'Cash withdrawal')
# User Group
UserGroup.create(name: 'Manager', desc: 'Can view TXNs, control users and generate balance sheet')
UserGroup.create(name: 'Accountant', desc: 'Can control TXNs')
# User
User.create(name: 'Hammad', email: 'hammad77@gmail.com', password: '123456', group_id: 1, encrypted_password: '$2a$10$/eYVnxH08f.5rXSJfJz74eYL2pEK.U0MW2hcOSJGRY2iMgSK7GPM.', salt: '$2a$10$/eYVnxH08f.5rXSJfJz74e')
User.create(name: 'Muhammad', email: 'muhammad@test.com', password: '123456', group_id: 2,  encrypted_password: '$2a$10$/eYVnxH08f.5rXSJfJz74eYL2pEK.U0MW2hcOSJGRY2iMgSK7GPM.', salt: '$2a$10$/eYVnxH08f.5rXSJfJz74e')

