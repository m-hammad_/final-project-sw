json.array!(@acc_transactions) do |acc_transaction|
  json.extract! acc_transaction, :id, :type_a, :type_a_acc, :type_b, :type_b_acc, :amount, :date, :desc
  json.url acc_transaction_url(acc_transaction, format: :json)
end
