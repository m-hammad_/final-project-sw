class AccTransactionsController < ApplicationController
  before_action :set_acc_transaction, only: [:show, :edit, :update, :destroy]

  # GET /acc_transactions
  # GET /acc_transactions.json
  def index
    @acc_transactions = AccTransaction.all
  end

  # GET /acc_transactions/1
  # GET /acc_transactions/1.json
  def show
  end

  # GET /acc_transactions/new
  def new
    @acc_transaction = AccTransaction.new
  end

  # GET /acc_transactions/1/edit
  def edit
  end

  # POST /acc_transactions
  # POST /acc_transactions.json
  def create
    @acc_transaction = AccTransaction.new(acc_transaction_params)
    succeeded = false

    AccTransaction.transaction do
      succeeded = @acc_transaction.save
      if succeeded
        Account.update_balances(acc_transaction_params[:type_a], acc_transaction_params[:type_a_acc],
                                acc_transaction_params[:type_b], acc_transaction_params[:type_b_acc], acc_transaction_params[:amount])
      end
    end

    respond_to do |format|
      # if @acc_transaction.save
      if succeeded
        format.html { redirect_to @acc_transaction, notice: 'Acc transaction was successfully created.' }
        format.json { render :show, status: :created, location: @acc_transaction }
      else
        format.html { render :new, notice: 'Acc transaction violates the predefined rules.' }
        format.json { render json: @acc_transaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /acc_transactions/1
  # PATCH/PUT /acc_transactions/1.json
  def update
    respond_to do |format|
      if @acc_transaction.update(acc_transaction_params)
        format.html { redirect_to @acc_transaction, notice: 'Acc transaction was successfully updated.' }
        format.json { render :show, status: :ok, location: @acc_transaction }
      else
        format.html { render :edit }
        format.json { render json: @acc_transaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /acc_transactions/1
  # DELETE /acc_transactions/1.json
  def destroy
    @acc_transaction.destroy
    respond_to do |format|
      format.html { redirect_to acc_transactions_url, notice: 'Acc transaction was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_acc_transaction
    @acc_transaction = AccTransaction.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def acc_transaction_params
    params.require(:acc_transaction).permit(:type_a, :type_a_acc, :type_b, :type_b_acc, :amount, :date, :desc)
  end
end
