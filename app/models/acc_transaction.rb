class AccTransaction < ActiveRecord::Base

  belongs_to :type_a_account, class_name: 'Account', :foreign_key => :type_a_acc
  belongs_to :type_b_account, class_name: 'Account', :foreign_key => :type_b_acc

  before_save :validate_txn

  def validate_txn
    type_a = self.type_a
    type_b = self.type_b
    temp_from = self.type_a_acc
    temp_to = self.type_b_acc

    amount_a = self.amount
    amount_b = self.amount
    if type_a.eql? 'from'
      amount_a = amount_a * -1
    end
    if type_b.eql? 'from'
      amount_b = amount_b * -1
    end

    sql_string = 'SELECT at.lhs_attr, at.rhs_attr, at.plus_attr, at.minus_attr
      from accounts ac, account_types at
      where ac.id in (' + temp_from.to_s + ',' + temp_to.to_s + ') and ac.account_type_id = at.id'

    types_details = AccTransaction.connection.execute(sql_string)

    lhs_plus = 0
    lhs_min = 0
    rhs_plus = 0
    rhs_min = 0

    types_details.each_with_index do |type, index|
      is_lhs = type['lhs_attr']
      is_plus = type['plus_attr']

      local_amount = 0
      if index == 0
        local_amount = amount_a
      else
        local_amount = amount_b
      end

      if is_lhs.eql? 't'
        if is_plus.eql? 't'
          lhs_plus = lhs_plus + local_amount
        else
          lhs_min = lhs_min + local_amount
        end
      else
        if is_plus.eql? 't'
          rhs_plus = rhs_plus + local_amount
        else
          rhs_min = rhs_min + local_amount
        end
      end
    end

    total_lhs = lhs_plus - lhs_min
    total_rhs = rhs_plus - rhs_min
    if total_lhs == total_rhs
      return true
    else
      return false
    end
  end

end
