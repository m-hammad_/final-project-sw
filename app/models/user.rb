class User < ActiveRecord::Base
  # attr_accessor :name, :username, :password, :email, :group_id

  attr_accessor :password

  validates :email, :presence => true, :uniqueness => true
  #password_confirmation attr
  validates :password, :confirmation => true
  validates_length_of :password, :in => 6..20, :on => :create

  belongs_to :user_group, :foreign_key => :group_id

  before_save :encrypt_password
  after_save :clear_password
  def encrypt_password
    if self.password.present?
      self.salt = BCrypt::Engine.generate_salt
      self.encrypted_password= BCrypt::Engine.hash_secret(password, salt)
      self.password = ' '
    end
  end
  def clear_password
    self.password = nil
  end

  def self.authenticate(username_or_email="", login_password="")
    user = User.find_by_email(username_or_email)
    if user && user.match_password(login_password)
      return user
    else
      return false
    end
  end

  def match_password(login_password="")
    encrypted_password == BCrypt::Engine.hash_secret(login_password, salt)
  end

end
