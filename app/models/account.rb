class Account < ActiveRecord::Base
  # attr_accessor :name, :account_type_id, :balance

  belongs_to :account_type

  # has_many :from_transactions, class_name: 'Transaction', :foreign_key => :from_acc
  # has_many :to_transactions, class_name: 'Transaction', :foreign_key => :to_acc

  has_many :from_transactions, class_name: 'AccTransaction', :foreign_key => :type_a_acc
  has_many :to_transactions, class_name: 'AccTransaction', :foreign_key => :type_b_acc

  def self.update_balances(type_a, type_a_ac, type_b, type_b_ac, amount)
    acc_a = Account.find(type_a_ac)
    acc_b = Account.find(type_b_ac)

    if type_a.eql? 'from'
      acc_a.balance = acc_a.balance - amount.to_f
    else
      acc_a.balance = acc_a.balance + amount.to_f
    end
    if type_b.eql? 'from'
      acc_b.balance = acc_b.balance - amount.to_f
    else
      acc_b.balance = acc_b.balance + amount.to_f
    end

    acc_a.save
    acc_b.save
  end

  # def self.get_balance_sheet
  #   balance_sheet_details = select("name, balance")
  #   print '-----------------------------------'
  #   print balance_sheet_details
  #   print '-----------------------------------'
  #   return balance_sheet_details
  # end

end
