class Transaction < ActiveRecord::Base
  # attr_accessor :from_acc, :to_acc, :amount, :date, :is_to_to, :desc

  belongs_to :from_account, class_name: 'Account', :foreign_key => :from_acc
  belongs_to :to_account, class_name: 'Account', :foreign_key => :to_acc

  before_save :validate_txn

  def validate_txn
    temp_from = self.from_acc
    temp_to = self.to_acc
    sql_string = 'SELECT at.lhs_attr, at.rhs_attr, at.plus_attr, at.minus_attr
      from accounts ac, account_types at
      where ac.id in (' + temp_from.to_s + ',' + temp_to.to_s + ') and ac.account_type_id = at.id'

    types_details = Transaction.connection.execute(sql_string)

    lhs_plus = 0
    lhs_min = 0
    rhs_plus = 0
    rhs_min = 0

    types_details.each_with_index do |type, index|
      is_lhs = type['lhs_attr']
      is_plus = type['plus_attr']

      from_amount = 1
      if index == 0 && self.is_to_to == 0
        from_amount = -1
      end

      if is_lhs.eql? 't'
        if is_plus.eql? 't'
          lhs_plus = lhs_plus + (self.amount * from_amount)
        else
          lhs_min = lhs_min + (self.amount * from_amount)
        end
      else
        if is_plus.eql? 't'
          rhs_plus = rhs_plus + (self.amount * from_amount)
        else
          rhs_min = rhs_min + (self.amount * from_amount)
        end
      end
    end

    total_lhs = lhs_plus - lhs_min
    total_rhs = rhs_plus - rhs_min
    if total_lhs == total_rhs
      return true
    else
      return false
    end
  end

  def self.get_balance_sheet
    balance_sheet_details = self.connection.execute('select sum(acc_sum) as type_sum, type_name_s, lhs, rhs, pls, min
      from( select sum(hist_sum) as acc_sum, acc_name, type_name as type_name_s, lhs, rhs, pls, min
      from (select sum(bs.amount) as hist_sum, ac.name as acc_name, act.name as type_name, act.lhs_attr as lhs,  act.rhs_attr as rhs, act.plus_attr as pls, act.minus_attr as min
      from balance_sheet_hist as bs , accounts as ac, account_types as act
      where bs.acc_id = ac.id and ac.account_type_id = act.id
      group by bs.acc_id)
      group by acc_name)
      group by type_name_s')

    return balance_sheet_details
  end

end
